#ifndef OBSTACLES_H
#define OBSTACLES_H
#include <HGE/HGE_Core.h>

typedef struct {
	float power;
} wind_fan;


// # # # # # # #
// #           #
// #           #
// #           #
// #           #
// #           #
// #           #
// #           #
// #           #
// #           #
// #           #
// # # # X # # #


void system_wind_fan(hge_entity* entity, hge_transform* transform, wind_fan* fan);

void system_obstacle_block(hge_entity* entity, hge_transform* transform, tag_component* obstacle);

#endif
