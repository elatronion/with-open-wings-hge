#ifndef GAME_MANAGER_H
#define GAME_MANAGER_H

#include <HGE/HGE_Core.h>

typedef struct {

} game_manager;

void setPlayerHat(int id);
int getPlayerHat();
void setPlayerHead();
int getPlayerHead();

char* getPlayerName();

void setPlayerBodyColor(hge_vec3 color);
void setPlayerHighlightColor(hge_vec3 color);
hge_vec3 getPlayerBodyColor();
hge_vec3 getPlayerHighlightColor();

void system_game_management(hge_entity* entity, game_manager* gm);

#endif
