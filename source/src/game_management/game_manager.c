#include "game_manager.h"
#include "level_manager.h"
#include "text.h"

#define NUM_HEADS 4
#define NUM_HATS 6

int hat_id = 0;
int head_id = 1;

char player_name[255];

hge_vec3 player_body_color;
hge_vec3 player_highlight_color;

void setPlayerHat(int id) {
  hat_id = id;
  hat_id = fmin(fmax(hat_id, 0), NUM_HATS);
}

int getPlayerHat() {
  return hat_id;
}

void setPlayerHead(int id) {
  head_id = id;
  head_id = fmin(fmax(head_id, 1), NUM_HEADS);
}

int getPlayerHead() {
  return head_id;
}

char* getPlayerName() {
  return &player_name;
}

void setPlayerBodyColor(hge_vec3 color) {
  player_body_color = color;
  player_body_color.x = fmin(fmax(player_body_color.x, 0), 1);
  player_body_color.y = fmin(fmax(player_body_color.y, 0), 1);
  player_body_color.z = fmin(fmax(player_body_color.z, 0), 1);
}

void setPlayerHighlightColor(hge_vec3 color) {
  player_highlight_color = color;
  player_highlight_color.x = fmin(fmax(player_highlight_color.x, 0), 1);
  player_highlight_color.y = fmin(fmax(player_highlight_color.y, 0), 1);
  player_highlight_color.z = fmin(fmax(player_highlight_color.z, 0), 1);
}

hge_vec3 getPlayerBodyColor() {
  return player_body_color;
}

hge_vec3 getPlayerHighlightColor() {
  return player_highlight_color;
}

void CheckKeyboardInput(char* str, size_t max_len) {
  for(int i = 0; i < HGE_KEY_LAST; i++) {
		if(hgeInputGetKeyDown(i)) {
       		// pressed i;
          if(i == 257) continue;
          if(i == 259) {
            if(strlen(str) <= 0) return;
            str[strlen(str)-1] = '\0';
          } else {
            if(strlen(str) >= max_len) return;
            str[strlen(str)] = i;
          }
		} else if(hgeInputGetKeyUp(i)) {
       		// released i;
		}
	}
}

void system_game_management(hge_entity* entity, game_manager* gm) {
  // check what player input

  CheckKeyboardInput(&player_name, 255);

  // Shadow
  jamTextRenderSimple(
    "insert player name", true,
    hgeVec3(0, 15, 0.9f),
    0.1f,
    hgeVec4(0, 0, 0, 1)
  );
  // Text
  jamTextRenderSimple(
    "insert player name", true,
    hgeVec3(0, 15, 2),
    0.1f,
    hgeVec4(1, 1, 1, 1)
  );

  // Shadow
  jamTextRenderSimple(
		player_name, true,
		hgeVec3(0, 0, 0.9f),
		0.1f,
		hgeVec4(0, 0, 0, 1)
	);
	// Text
	jamTextRenderSimple(
		player_name, true,
		hgeVec3(0, 0, 2),
		0.1f,
		hgeVec4(1, 1, 1, 1)
	);

  if(hgeInputGetKeyDown(HGE_KEY_ENTER)) {
    hgeDestroyEntity(entity);
    load_bird_place();
  }

}
