#ifndef RACE_H
#define RACE_H
#include <HGE/HGE_Core.h>
#include <stdio.h>
#include <math.h>

typedef struct s_checkpoint {
	hge_vec3 location;
	struct s_checkpoint* next;
} checkpoint;

typedef struct {
	float timer;
	float countdown;
	checkpoint* root_checkpoint;
	checkpoint* cur_checkpoint;
} race_component;

void push_checkpoint(checkpoint* root_checkpoint, hge_vec3 location);
void clean_checkpoints(checkpoint* root_checkpoint);

void race_system(hge_entity* entity, race_component* race);

#endif
