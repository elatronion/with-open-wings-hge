#include <HGE/HGE_Core.h>
#include <stdio.h>
#include <math.h>
#include "jamutils.h"
#include "bird.h"
#include "background.h"
#include "race.h"
#include "obstacles.h"
#include "level_manager.h"
#include "text.h"
#include "game_manager.h"

int main() {

   /* Intializes random number generator */
	 time_t t;
   srand((unsigned) time(&t));

	 setPlayerBodyColor(hgeVec3(RandomFloat(0, 255)/255.f, RandomFloat(0, 255)/255.f, RandomFloat(0, 255)/255.f));
	 setPlayerHighlightColor(hgeVec3(0, 0, 0));

	 setPlayerHead((int)RandomFloat(1, 5));


	hge_window window = { "With Open Wings", 1280, 720 };
	hgeInit(60, window, HGE_INIT_RENDERING | HGE_INIT_ECS);

	hgeResourcesLoadShader("res/shaders/gui.vs", NULL, "res/shaders/gui.fs", "gui");
	hge_shader gui_shader = hgeResourcesQueryShader("gui");
	hgeUseShader(gui_shader);
	hgeShaderSetInt(gui_shader, "material.diffuse", 0);
	hgeShaderSetInt(gui_shader, "material.normal", 1);

	hgeResourcesLoadTexture("res/HGE/DEFAULT NORMAL.png", "HGE DEFAULT NORMAL");
	//hgeResourcesLoadTexture("res/chirp.png", "CHIRP");
	//hgeResourcesLoadTexture("res/chirp fly.png", "CHIRP WINGS OPENED");
	//hgeResourcesLoadTexture("res/chirp fly (closed wings).png", "CHIRP WINGS CLOSED");
	hgeResourcesLoadTexture("res/bird/bird body.png", "BIRD BODY");
	hgeResourcesLoadTexture("res/bird/bird fly body.png", "BIRD BODY WINGS OPENED");
	hgeResourcesLoadTexture("res/bird/bird fly body (closed wings).png", "BIRD BODY WINGS CLOSED");

	// HEADS
	hgeResourcesLoadTexture("res/bird/bird head 1.png", "BIRD HEAD 1");
	hgeResourcesLoadTexture("res/bird/bird fly head 1 (wings).png", "BIRD HEAD 1 WINGS");
	hgeResourcesLoadTexture("res/bird/bird head 2.png", "BIRD HEAD 2");
	hgeResourcesLoadTexture("res/bird/bird fly head 2 (wings).png", "BIRD HEAD 2 WINGS");
	hgeResourcesLoadTexture("res/bird/bird head 3.png", "BIRD HEAD 3");
	hgeResourcesLoadTexture("res/bird/bird fly head 3 (wings).png", "BIRD HEAD 3 WINGS");
	hgeResourcesLoadTexture("res/bird/bird head 4.png", "BIRD HEAD 4");
	hgeResourcesLoadTexture("res/bird/bird fly head 4 (wings).png", "BIRD HEAD 4 WINGS");

	// HATS
	hgeResourcesLoadTexture("res/bird/hats/bird hat 1.png", "BIRD HAT 1");
	hgeResourcesLoadTexture("res/bird/hats/bird hat 1 (flight).png", "BIRD HAT 1 FLIGHT");
	hgeResourcesLoadTexture("res/bird/hats/bird hat 2.png", "BIRD HAT 2");
	hgeResourcesLoadTexture("res/bird/hats/bird hat 1 (flight).png", "BIRD HAT 2 FLIGHT");
	hgeResourcesLoadTexture("res/bird/hats/bird hat 3.png", "BIRD HAT 3");
	hgeResourcesLoadTexture("res/bird/hats/bird hat 3 (flight).png", "BIRD HAT 3 FLIGHT");
	hgeResourcesLoadTexture("res/bird/hats/bird hat 4.png", "BIRD HAT 4");
	hgeResourcesLoadTexture("res/bird/hats/bird hat 4 (flight).png", "BIRD HAT 4 FLIGHT");
	hgeResourcesLoadTexture("res/bird/hats/bird hat 5.png", "BIRD HAT 5");
	hgeResourcesLoadTexture("res/bird/hats/bird hat 5 (flight).png", "BIRD HAT 5 FLIGHT");
	hgeResourcesLoadTexture("res/bird/hats/bird hat 6.png", "BIRD HAT 6");
	hgeResourcesLoadTexture("res/bird/hats/bird hat 6 (flight).png", "BIRD HAT 6 FLIGHT");

	hgeResourcesLoadShader("res/shaders/bird_color.vs", NULL, "res/shaders/bird_color.fs", "bird");


	hgeResourcesLoadTexture("res/feather.png", "FEATHER");


	hgeResourcesLoadTexture("res/cloud_layer1.png", "cloud_layer1");
	hgeResourcesLoadTexture("res/cloud_layer2.png", "cloud_layer2");
	hgeResourcesLoadTexture("res/cloud_layer3.png", "cloud_layer3");
	hgeResourcesLoadTexture("res/cloud_layer4.png", "cloud_layer4");

	hgeResourcesLoadTexture("res/bird-game-place.png", "PLACE");
	//hgeResourcesLoadTexture("res/level1.png", "LEVEL 1");

	hgeResourcesLoadTexture("res/block/center.png", "BLOCK CENTER");
	hgeResourcesLoadTexture("res/block/corner.png", "BLOCK CORNER");

	hgeResourcesLoadShader("res/shaders/text.vs", NULL, "res/shaders/text.fs", "text");
	jamTextCreate("res/fonts/slkscr.ttf");

	hgeAddBaseSystems();
	hgeAddSystem(system_bird_character, 2, "transform", "bird");
	hgeAddSystem(system_parallax, 3, "transform", "sprite", "parallax");
	hgeAddSystem(system_obstacle_block, 2, "transform", "obstacle");
	//hgeAddSystem(system_wind_fan, 2, "transform", "wind fan");

	hgeAddSystem(race_system, 1, "race");
	hgeAddSystem(system_bird_renderer, 2, "transform", "bird");
	hgeAddSystem(system_bird_controller, 2, "transform", "bird");

	hgeAddSystem(system_game_management, 1, "game manager");

	hge_entity* camera_entity = hgeCreateEntity();
	hge_camera cam = {true, true, 1.f/6.f, hgeWindowWidth(), hgeWindowHeight(), (float)hgeWindowWidth() / (float)hgeWindowHeight(), -500.0f, 500.0f };
	hge_vec3 camera_position = { 0, 0, 0 };
	orientation_component camera_orientation = {0.0f, -90.0f, 0.0f};
	hgeAddComponent(camera_entity, hgeCreateComponent("Camera", &cam, sizeof(cam)));
	hgeAddComponent(camera_entity, hgeCreateComponent("Position", &camera_position, sizeof(camera_position)));
	hgeAddComponent(camera_entity, hgeCreateComponent("Orientation", &camera_orientation, sizeof(camera_orientation)));
	tag_component activecam_tag;
	hgeAddComponent(camera_entity, hgeCreateComponent("ActiveCamera", &activecam_tag, sizeof(activecam_tag)));




	/*
	// 4k Level Image
	hge_entity* level_entity = hgeCreateEntity();
	hge_transform level_transform;
	level_transform.position = hgeVec3(200 + 3840/2, 300 - 2160/2, -0.5f);
	level_transform.scale = hgeVec3(3840, 2160, 0);
	hgeAddComponent(level_entity, hgeCreateComponent("Transform", &level_transform, sizeof(level_transform)));
	hge_texture level_sprite = hgeResourcesQueryTexture("LEVEL 1");
	hgeAddComponent(level_entity, hgeCreateComponent("Sprite", &level_sprite, sizeof(level_sprite)));
	*/

	/*
	// Race
	hge_entity* race_entity = hgeCreateEntity();
	race_component race;
	race.root_checkpoint.next = NULL;
	//push_checkpoint(&race.root_checkpoint, hgeVec3(200, 300, 0));
	//push_checkpoint(&race.root_checkpoint, hgeVec3(600, 300, 0));

	for(int i = 0; i < 50; i++) {
		push_checkpoint(&race.root_checkpoint, hgeVec3(200 + i*300, 300 + cos(i)*200, 0));
	}

	race.cur_checkpoint = race.root_checkpoint.next;
	hgeAddComponent(race_entity, hgeCreateComponent("race", &race, sizeof(race)));
	*/

	load_menu();
	//load_bird_place();
	//level_load("res/levels/bird_level_2.tmx");

	// block obstacle
	/*
	hge_entity* block_entity = hgeCreateEntity();
	hge_transform block_transform = { hgeVec3(300, 300, 0), hgeVec3(128, 128*4, 0), hgeVec3(0, 0, 0) };
	hgeAddComponent(block_entity, hgeCreateComponent("transform", &block_transform, sizeof(block_transform)));
	tag_component obstacle;
	hgeAddComponent(block_entity, hgeCreateComponent("obstacle", &obstacle, sizeof(obstacle)));
	*/

	/*
	// wind obstacle
	hge_entity* wind_fan_entity = hgeCreateEntity();
	hge_transform wind_fan_transform = { hgeVec3(300, 300, 0), hgeVec3(128, 128*4, 0), hgeVec3(0, 0, 0) };
	hgeAddComponent(wind_fan_entity, hgeCreateComponent("transform", &wind_fan_transform, sizeof(wind_fan_transform)));
	wind_fan wf = { 1000.f };
	hgeAddComponent(wind_fan_entity, hgeCreateComponent("wind fan", &wf, sizeof(wf)));
	*/

	// background
	for(int i = 4; i >= 1; i--) {
	for(int y = -2; y < 2; y++)
	for(int x = -2; x < 2; x++) {
			hge_entity* bg_entity = hgeCreateEntity();
			hge_transform bg_transform;
			bg_transform.position = hgeVec3(500/2 + 500*x, -250/2 + -250*y, -10+ i-5);
			bg_transform.scale = hgeVec3(500, 250, 0);
			bg_transform.rotation = hgeVec3(0, 0, 0);
			hgeAddComponent(bg_entity, hgeCreateComponent("transform", &bg_transform, sizeof(bg_transform)));
			char text_str[] = "cloud_layer1";
			sprintf(&text_str, "%s%d", "cloud_layer", 5-i);
			hge_texture bg_sprite = hgeResourcesQueryTexture(text_str);
			hgeAddComponent(bg_entity, hgeCreateComponent("sprite", &bg_sprite, sizeof(bg_sprite)));
			parallax_component parallax = { 1+i*0.5f };
			hgeAddComponent(bg_entity, hgeCreateComponent("parallax", &parallax, sizeof(parallax)));
		}
	}


	load_level_folder();
	hgeStart();
	clean_loaded_level_files();

	return 0;
}
