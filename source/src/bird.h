#ifndef BIRD_H
#define BIRD_H
#include <HGE/HGE_Core.h>
#include <stdio.h>
#include <math.h>
#include "jamutils.h"

typedef struct {
  float time;
} level_data;

void load_level_folder();
void clean_loaded_level_files();


typedef enum {
	IDLE, WALKING, FALLING, FLYING,
	MENU_BARBER, MENU_HATS, SELECTING_LEVEL,
  RACE_COUNTDOWN, RACE_END
} character_state;

typedef struct {
	character_state state;
	float angle;
	float desired_angle;
	bool flip_h;

	float thrust;
	float weight;
	hge_vec2 wind;

	float desired_fov;

	bool angle_overflow;
	float angle_offset;

	hge_vec3 velocity;
	hge_transform rendered_transform;

  hge_vec3 body_color;
  hge_vec3 highlight_color;
} character_bird;

void system_bird_character(hge_entity* entity, hge_transform* transform, character_bird* bird);
void system_bird_renderer(hge_entity* entity, hge_transform* transform, character_bird* bird);
void system_bird_controller(hge_entity* entity, hge_transform* transform, character_bird* bird);

#endif
