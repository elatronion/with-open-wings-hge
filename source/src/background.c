#include "background.h"

void system_parallax(hge_entity* entity, hge_transform* transform, hge_texture* sprite, parallax_component* parallax) {
	hge_vec3* camera_position = NULL;
	hge_ecs_request active_camera_request = hgeECSRequest(1, "ActiveCamera");
	for(int i = 0; i < active_camera_request.NUM_ENTITIES; i++) {
		hge_entity* cam_entity = active_camera_request.entities[i];
		camera_position = cam_entity->components[hgeQuery(cam_entity, "Position")].data;
	}

	float increase = 1/(parallax->parallax_amnt-1) + 1;
	int x_offset = (int)((camera_position->x/(transform->scale.x*increase)));
	int y_offset = (int)((camera_position->y/(transform->scale.y*increase)));

	hge_vec3 rendered_position = transform->position;
	rendered_position.x += x_offset * transform->scale.x;
	rendered_position.y += y_offset * transform->scale.y;

	rendered_position.x = rendered_position.x + camera_position->x/parallax->parallax_amnt;
	rendered_position.y = rendered_position.y + camera_position->y/parallax->parallax_amnt;
	hge_material material = { *sprite, hgeResourcesQueryTexture("HGE DEFAULT NORMAL") };
	hgeRenderSprite(hgeResourcesQueryShader("sprite_shader"), material, rendered_position, transform->scale, 0.0f);
}
