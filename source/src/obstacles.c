#include "obstacles.h"
#include "bird.h"

void system_wind_fan(hge_entity* entity, hge_transform* transform, wind_fan* fan) {

	hge_transform* player_transform = NULL;
	character_bird* player_bird = NULL;
	hge_ecs_request player_request = hgeECSRequest(1, "player");
	for(int i = 0; i < player_request.NUM_ENTITIES; i++) {
		hge_entity* player_entity = player_request.entities[i];
		player_transform = player_entity->components[hgeQuery(player_entity, "transform")].data;
		player_bird = player_entity->components[hgeQuery(player_entity, "bird")].data;
	}

	if(AABB(*player_transform, *transform)) {
		// Intersection
		//printf("Windersection\n");
		player_bird->wind.y += fan->power * hgeDeltaTime();
	}

	hge_material material = { hgeResourcesQueryTexture("HGE DEFAULT NORMAL"), hgeResourcesQueryTexture("HGE DEFAULT NORMAL") };
	hgeRenderSprite(hgeResourcesQueryShader("sprite_shader"), material, transform->position, transform->scale, transform->rotation.z);
}

void system_obstacle_block(hge_entity* entity, hge_transform* transform, tag_component* obstacle) {
	hge_transform* player_transform = NULL;
	character_bird* player_bird = NULL;
	hge_ecs_request player_request = hgeECSRequest(1, "player");
	if(player_request.NUM_ENTITIES < 1) return;
	for(int i = 0; i < player_request.NUM_ENTITIES; i++) {
		hge_entity* player_entity = player_request.entities[i];
		player_transform = player_entity->components[hgeQuery(player_entity, "transform")].data;
		player_bird = player_entity->components[hgeQuery(player_entity, "bird")].data;
	}

	if(AABB(*player_transform, *transform)) {
		// Intersection
		player_bird->thrust = 0;

		if(player_transform->position.y > transform->position.y + transform->scale.y/2 - 4) {
			player_bird->weight = 0;
		} else if(player_transform->position.y < transform->position.y - transform->scale.y/2 + 4) {

		} else {
			if(player_transform->position.x < transform->position.x) {
				player_transform->position.x = transform->position.x - transform->scale.x/2 - player_transform->scale.x/2 - 1;
			} else {
				player_transform->position.x = transform->position.x + transform->scale.x/2 + player_transform->scale.x/2 + 1;
			}
		}
	}


	// Rendering
	hge_material material_center = { hgeResourcesQueryTexture("BLOCK CENTER"), hgeResourcesQueryTexture("HGE DEFAULT NORMAL") };
	hge_material material_corner = { hgeResourcesQueryTexture("BLOCK CORNER"), hgeResourcesQueryTexture("HGE DEFAULT NORMAL") };
	// Corners
	hge_transform corner_tl =
	{
		hgeVec3(
			transform->position.x - transform->scale.x/2 + 8,
			transform->position.y + transform->scale.y/2 - 8,
			0.0f
		),
		hgeVec3(16, 16, 0), hgeVec3(0, 0, 0)
	};
	hge_transform corner_tr =
	{
		hgeVec3(
			transform->position.x + transform->scale.x/2 - 8,
			transform->position.y + transform->scale.y/2 - 8,
			0.0f
		),
		hgeVec3(-16, 16, 0), hgeVec3(0, 0, 0)
	};
	hge_transform corner_br =
	{
		hgeVec3(
			transform->position.x - transform->scale.x/2 + 8,
			transform->position.y - transform->scale.y/2 + 8,
			0.0f
		),
		hgeVec3(16, -16, 0), hgeVec3(0, 0, 0)
	};
	hge_transform corner_bl =
	{
		hgeVec3(
			transform->position.x + transform->scale.x/2 - 8,
			transform->position.y - transform->scale.y/2 + 8,
			0.0f
		),
		hgeVec3(-16, -16, 0), hgeVec3(0, 0, 0)
	};

	// Sides
	hge_transform left_t =
	{
		hgeVec3(
			transform->position.x - transform->scale.x/2 + 8,
			transform->position.y,
			0.0f
		),
		hgeVec3(16, transform->scale.y-16*2, 0), hgeVec3(0, 0, 0)
	};

	hge_transform right_t =
	{
		hgeVec3(
			transform->position.x + transform->scale.x/2 - 8,
			transform->position.y,
			0.0f
		),
		hgeVec3(16, transform->scale.y-16*2, 0), hgeVec3(0, 0, 0)
	};

	hge_transform top_t =
	{
		hgeVec3(
			transform->position.x,
			transform->position.y  + transform->scale.y/2 - 8,
			0.0f
		),
		hgeVec3(transform->scale.x-16*2, 16, 0), hgeVec3(0, 0, 0)
	};

	hge_transform bottom_t =
	{
		hgeVec3(
			transform->position.x,
			transform->position.y  - transform->scale.y/2 + 8,
			0.0f
		),
		hgeVec3(transform->scale.x-16*2, 16, 0), hgeVec3(0, 0, 0)
	};

	// Center
	hge_transform center_t =
	{
		hgeVec3(
			transform->position.x,
			transform->position.y,
			0.0f
		),
		hgeVec3(transform->scale.x-16*2, transform->scale.y-16*2, 0), hgeVec3(0, 0, 0)
	};

	hgeRenderSprite(hgeResourcesQueryShader("sprite_shader"), material_corner, corner_tl.position, corner_tl.scale, corner_tl.rotation.z);
	hgeRenderSprite(hgeResourcesQueryShader("sprite_shader"), material_corner, corner_tr.position, corner_tr.scale, corner_tr.rotation.z);
	hgeRenderSprite(hgeResourcesQueryShader("sprite_shader"), material_corner, corner_bl.position, corner_bl.scale, corner_bl.rotation.z);
	hgeRenderSprite(hgeResourcesQueryShader("sprite_shader"), material_corner, corner_br.position, corner_br.scale, corner_br.rotation.z);

	hgeRenderSprite(hgeResourcesQueryShader("sprite_shader"), material_center, left_t.position, left_t.scale, left_t.rotation.z);
	hgeRenderSprite(hgeResourcesQueryShader("sprite_shader"), material_center, right_t.position, right_t.scale, right_t.rotation.z);
	hgeRenderSprite(hgeResourcesQueryShader("sprite_shader"), material_center, top_t.position, top_t.scale, top_t.rotation.z);
	hgeRenderSprite(hgeResourcesQueryShader("sprite_shader"), material_center, bottom_t.position, bottom_t.scale, bottom_t.rotation.z);


	hgeRenderSprite(hgeResourcesQueryShader("sprite_shader"), material_center, center_t.position, center_t.scale, center_t.rotation.z);

	//hgeRenderSprite(hgeResourcesQueryShader("sprite_shader"), material_center, transform->position, transform->scale, transform->rotation.z);
}
