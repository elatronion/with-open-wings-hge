#include "level_manager.h"
#include "obstacles.h"
#include "race.h"
#include "bird.h"
#include "tmx.h"
#include "game_manager.h"
#include "jamutils.h"

#define MAX_RACE_ENTITIES 250

int num_race_entities = 0;
hge_entity* race_entities[MAX_RACE_ENTITIES];

void add_to_entity_list(hge_entity* entity) {
    race_entities[num_race_entities] = entity;
    num_race_entities++;
}

void ParseTMXBlock(tmx_object* object) {
  hge_entity* block_entity = hgeCreateEntity();
  hge_transform block_transform = { hgeVec3(object->x + object->width/2, -object->y - object->height/2, 0), hgeVec3(object->width, object->height, 0), hgeVec3(0, 0, 0) };
  hgeAddComponent(block_entity, hgeCreateComponent("transform", &block_transform, sizeof(block_transform)));
  tag_component obstacle;
  hgeAddComponent(block_entity, hgeCreateComponent("obstacle", &obstacle, sizeof(obstacle)));
  add_to_entity_list(block_entity);
}

int num_checkpoints = 0;

int ParseTMXCheckpoint(tmx_object* object) {
  int checkpoint_id = -1;

  sscanf(object->name, "%*s %d", &checkpoint_id);
  printf("Checkpoint id: %d\n", checkpoint_id);
  return checkpoint_id;
}

checkpoint* race_root = NULL;

void ParseTMXData(tmx_map* map) {
  tmx_layer* layer = map->ly_head;
  while(layer != NULL) {
    if(strcmp(layer->name, "Block Obstacles") == 0) {
      tmx_object_group* object_group = layer->content.objgr;
      tmx_object* object = object_group->head;
      while(object != NULL) {
        ParseTMXBlock(object);
        object = object->next;
      }
    } else if(strcmp(layer->name, "Checkpoints") == 0) {

      hge_entity* race_entity = hgeCreateEntity();
      race_component race;
      race.timer = 0;
      race.countdown = 3;
      race.root_checkpoint = malloc(sizeof(checkpoint));
      race.root_checkpoint->next = NULL;

      tmx_object_group* object_group = layer->content.objgr;
      tmx_object* object = object_group->head;
      num_checkpoints=0;
      while(object != NULL) { object = object->next; num_checkpoints++; }
      object = object_group->head;
      hge_vec3 checkpoints[num_checkpoints];
      while(object != NULL) {
        checkpoints[ParseTMXCheckpoint(object)] = hgeVec3(object->x, -object->y, 0.0f);
        object = object->next;
      }

      for(int i = 0; i < num_checkpoints; i++) {
        push_checkpoint(race.root_checkpoint, checkpoints[i]);
      }

      race.cur_checkpoint = race.root_checkpoint->next;
    	hgeAddComponent(race_entity, hgeCreateComponent("race", &race, sizeof(race)));

    	race_component* race_c = NULL;
    	hge_ecs_request race_request = hgeECSRequest(1, "race");
    	for(int i = 0; i < race_request.NUM_ENTITIES; i++) {
    		hge_entity* race_entity = race_request.entities[i];
    		race_c = race_entity->components[hgeQuery(race_entity, "race")].data;
    	}
      race_root = race.root_checkpoint;
      add_to_entity_list(race_entity);

    } else if(strcmp(layer->name, "Player") == 0) {

      tmx_object_group* object_group = layer->content.objgr;
      tmx_object* object = object_group->head;
      while(object != NULL) {
        create_player_bird(hgeVec3(object->x, -object->y, 0));
        object = object->next;
      }

    }
    layer = layer->next;
  }
}

char cur_level[255];

char* cur_level_str() {
  return &cur_level;
}

int cur_level_hash() {
  int hash = 0;
  for(int i = 0; i < strlen(cur_level); i++) {
    hash += cur_level[i];
  }
  return hash;
}

void level_unload() {
  if(race_root)
    clean_checkpoints(race_root);
  race_root = NULL;

  printf("destroy %d entities...\n", num_race_entities);
  for(int i = 0; i < num_race_entities; i++) {
    printf("destroy entity[%d]\n", i);
    hgeDestroyEntity(race_entities[i]);
    race_entities[i] = NULL;
  }
  num_race_entities = 0;
  //strcpy(&cur_level, "");
}

void level_load(const char* level_path) {
	level_unload();

  printf("Loading \"%s\"\n", level_path);

  strcpy(&cur_level, level_path);

  tmx_map *map = tmx_load(level_path);
  if (!map) {
		tmx_perror("Cannot load map");
    HGE_ERROR("Cannot Load Map \"%s\"", level_path);
    //LoadLevel("res/levels/error.tmx");
		return;
	}

  ParseTMXData(map);
  tmx_map_free(map);
}

void level_reload() {
  level_load(cur_level);
}

void create_player_bird(hge_vec3 position) {
	hge_entity* entity = hgeCreateEntity();
	hge_transform transform;
	//transform.position = hgeVec3(200, 300, 0);
	transform.position = position;
	transform.scale = hgeVec3(16, 16, 0);
	transform.rotation = hgeVec3(0, 0, 0);
	hgeAddComponent(entity, hgeCreateComponent("transform", &transform, sizeof(transform)));
	character_bird bird;
	bird.state = RACE_COUNTDOWN;
	bird.desired_angle = 90.0f;
	bird.angle = bird.desired_angle;
	bird.flip_h = false;
	bird.velocity = hgeVec3(0, 0, 0);
	bird.thrust = 0;
	bird.weight = 0;
  bird.angle_overflow = false;
	bird.angle_offset = 0.0f;
	bird.wind = hgeVec2(0, 0);
	bird.rendered_transform = transform;

  bird.body_color = getPlayerBodyColor();
  bird.highlight_color = getPlayerHighlightColor();

	hgeAddComponent(entity, hgeCreateComponent("bird", &bird, sizeof(bird)));
	tag_component player;
	hgeAddComponent(entity, hgeCreateComponent("player", &player, sizeof(player)));
  add_to_entity_list(entity);
}

/// //// /// //// ///
void load_menu() {
  level_unload();
  hge_entity* entity = hgeCreateEntity();
  game_manager gm;
  hgeAddComponent(entity, hgeCreateComponent("game manager", &gm, sizeof(gm)));
  add_to_entity_list(entity);
}

void load_bird_place() {
  level_unload();
  strcpy(&cur_level, "bird_place");
  // Chirp
  hge_entity* entity = hgeCreateEntity();
  hge_transform transform;
  //transform.position = hgeVec3(200, 300, 0);
  transform.position = hgeVec3(250, 300, 0);
  transform.scale = hgeVec3(16, 16, 0);
  transform.rotation = hgeVec3(0, 0, 0);
  hgeAddComponent(entity, hgeCreateComponent("transform", &transform, sizeof(transform)));
  character_bird bird;
  bird.state = IDLE;
  bird.desired_angle = 0.0f;
  bird.angle = bird.desired_angle;
  bird.flip_h = false;
  bird.velocity = hgeVec3(0, 0, 0);
  bird.thrust = 0;
  bird.weight = 0;
  bird.wind = hgeVec2(0, 0);
  bird.desired_fov = (1.f/6);
  bird.rendered_transform = transform;

  bird.body_color = getPlayerBodyColor();
  bird.highlight_color = getPlayerHighlightColor();

  hgeAddComponent(entity, hgeCreateComponent("bird", &bird, sizeof(bird)));
  tag_component player;
  hgeAddComponent(entity, hgeCreateComponent("player", &player, sizeof(player)));
  add_to_entity_list(entity);

  // Bird Place
  hge_entity* place_entity = hgeCreateEntity();
  hge_transform place_transform;
  place_transform.position = hgeVec3(200, 300, -1);
  place_transform.scale = hgeVec3(800, 600, 0);
  hgeAddComponent(place_entity, hgeCreateComponent("Transform", &place_transform, sizeof(place_transform)));
  hge_texture place_sprite = hgeResourcesQueryTexture("PLACE");
  hgeAddComponent(place_entity, hgeCreateComponent("Sprite", &place_sprite, sizeof(place_sprite)));
  add_to_entity_list(place_entity);
}
