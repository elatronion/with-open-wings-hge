#ifndef LEVEL_MANAGER_H
#define LEVEL_MANAGER_H
#include <HGE/HGE_Core.h>

typedef struct {

} level;

char* cur_level_str();
int cur_level_hash();

void level_unload();
void level_load(const char* level_path);
void level_reload();

void create_player_bird(hge_vec3 position);


#endif
