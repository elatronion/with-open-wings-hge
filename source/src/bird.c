#include "bird.h"
#include "level_manager.h"
#include "text.h"
#include "game_manager.h"

#define clamp(value, min_value, max_value) ( fmin(max_value, fmax(min_value, value)) )

/// /// ///

#include "dirent.h"

typedef struct level_file {
  char title[255];
  level_data lvl_data;
  struct level_file * next;
} level_file_t;

level_file_t* head = NULL;

void sort_level_list() {
  bool ordered = false;
	level_file_t* cur = head;
  while(!ordered) {
    ordered = true;
    cur = head;
    while(cur) {

      level_file_t* FIRST = cur;
      level_file_t* SECOND = NULL;
      level_file_t* THIRD = NULL;

      if(FIRST)
        SECOND = FIRST->next;
      if(SECOND)
        THIRD = SECOND->next;

      if(SECOND && THIRD)
      if(SECOND->title[0] > THIRD->title[0]) {
        // Swap
        SECOND->next = THIRD->next;
        THIRD->next = SECOND;
        FIRST->next = THIRD;
        ordered = false;
      }

      cur = cur->next;
    }
  }
  printf("DONE!\n");
}

int num_level_files() {
	int count = 0;
	level_file_t* cur = head;
	while(cur->next) {
		cur = cur->next;
		count++;
	}
	return count;
}

char* get_level_title(int index) {
	int count = 0;
	level_file_t* cur = head;
	while(cur->next) {
		cur = cur->next;
		if(count == index) break;
		count++;
	}
	return &cur->title;
}

level_data get_level_data(int index) {
	int count = 0;
	level_file_t* cur = head;
	while(cur->next) {
		cur = cur->next;
		if(count == index) break;
		count++;
	}
	return cur->lvl_data;
}

level_data load_data_file(const char* title) {
  level_data lvl_data = { 0.0f };
  char path[255];
  sprintf(path, "%s%s%s", "res/levels/scores/", title, ".bin");
  binread(path, &lvl_data, sizeof(lvl_data));
  return lvl_data;
}

void add_level_file(const char* title) {
	//title[strlen(title)-3] = "\0";
  if(!strcmp(title, "level selection.tmx")) return;
	printf("Found Level File '%s'\n", title);
	level_file_t* cur = head;
	while(cur->next)
		cur = cur->next;
	cur->next = (level_file_t*)malloc(sizeof(level_file_t));
	cur = cur->next;
	strcpy(cur->title, title);
	cur->title[strlen(cur->title)-4] = '\0';
  cur->lvl_data = load_data_file(cur->title);
	cur->next = NULL;
}

void clean_loaded_level_files() {
	level_file_t* cur = head->next;
	while(cur) {
		level_file_t* tmp = cur;
		cur = cur->next;
		free(tmp);
	}
	free(head);
}

void load_level_folder() {
	head = (level_file_t*)malloc(sizeof(level_file_t));
	head->next = NULL;

	DIR *dir;
	struct dirent *ent;
	if ((dir = opendir ("res/levels/")) != NULL) {
		while ((ent = readdir (dir)) != NULL) {
			char *dot = strrchr(ent->d_name, '.');
			if (dot && !strcmp(dot, ".tmx"))
				add_level_file(ent->d_name);
		}
		closedir (dir);
	} else {
		perror ("");
		return EXIT_FAILURE;
	}

  sort_level_list();
}

/// /// ///

float STALL_SPEED = 100;
float MAX_THRUST = 1000;
float MAX_WEIGHT = 500;
			 float drag = 0.0f;

void system_bird_character(hge_entity* entity, hge_transform* transform, character_bird* bird) {
	float gravity = 250;
	float max_speed = 1000;
	switch(bird->state) {
		case FLYING:
		 {
			 // TODO: fix bird 360 rotation
			 float angle_speed = 5;
			 float delta_angle = (bird->angle - bird->desired_angle - bird->angle_offset);
			 if(!bird->angle_overflow && fabs(delta_angle) > 180) {
				 if(delta_angle>0)
				 	bird->angle_offset += 360;
				else
					bird->angle_offset -= 360;
				 bird->angle_overflow = true;
			 } else if(bird->angle_overflow) {
				 if(delta_angle < 180)
				 	bird->angle_overflow = false;
			 }
			 bird->angle += (bird->desired_angle+bird->angle_offset - bird->angle) * angle_speed * hgeDeltaTime();


			 bool wings_are_spread = !hgeInputGetMouse(HGE_MOUSE_LEFT);

			 /*hge_vec3 up_vector = hgeVec3(
				 sinf(hgeMathRadians(bird->angle-90)),
				 fabs(cosf(hgeMathRadians(bird->angle-90))),
				 0
			 );
			 up_vector.x = up_vector.x * bird->lift;
			 up_vector.y = up_vector.y * bird->lift;*/

			 hge_vec3 foward_vector = hgeVec3(
				 sinf(hgeMathRadians(bird->angle)),
				 cosf(hgeMathRadians(bird->angle)),
				 0
			 );
			 foward_vector.x = foward_vector.x * bird->thrust;
			 foward_vector.y = foward_vector.y * bird->thrust;

			 hge_vec3 back_vector = hgeVec3(
				 sinf(hgeMathRadians(bird->angle-180)),
				 cosf(hgeMathRadians(bird->angle-180)),
				 0
			 );
			 back_vector.x = back_vector.x * drag;
			 back_vector.y = back_vector.y * drag;

			 if(cos(hgeMathRadians(bird->angle)) > 0) {
				 // Going Up
				 float ammnt_of_thrust = fabs(cosf(hgeMathRadians(bird->angle)));
				 bird->thrust -= (ammnt_of_thrust * gravity)*hgeDeltaTime();
			 } else {
				 // Going Down
				 float ammnt_of_thrust = fabs(cosf(hgeMathRadians(bird->angle)));
				 bird->thrust += (ammnt_of_thrust * gravity)*hgeDeltaTime();
			 }

			 if(bird->thrust < STALL_SPEED) {
				 bird->weight -= gravity * hgeDeltaTime();
				 if(bird->thrust < STALL_SPEED && hgeInputGetMouseDown(HGE_MOUSE_LEFT)) { // FLAP WINGS
					 bird->thrust = 200;
					 bird->weight = 0;
				 }
			 } else {
				 if(!wings_are_spread) {
					 bird->weight -= gravity * hgeDeltaTime();
				 }
			 }
			 if(wings_are_spread) {
				 float ammnt_of_thrust = fabs(sinf(hgeMathRadians(bird->angle)));
				 bird->thrust += (ammnt_of_thrust * -bird->weight) * hgeDeltaTime();
				 bird->weight += (ammnt_of_thrust * bird->thrust) * hgeDeltaTime();
			 }

			 bird->thrust = fmin(MAX_THRUST, fmax(0, bird->thrust));
			 bird->weight = fmax(-MAX_WEIGHT, fmin(0, bird->weight));
			 //bird->lift -= gravity * hgeDeltaTime();
			 //bird->lift = fmax(0, bird->lift);

			 bird->velocity.x = foward_vector.x + back_vector.x;
			 bird->velocity.y = foward_vector.y + back_vector.y + bird->weight;
		 }
		case FALLING:
			bird->velocity.y = clamp(bird->velocity.y, -max_speed, max_speed);
			transform->position.x += bird->velocity.x*hgeDeltaTime();
			transform->position.y += bird->velocity.y*hgeDeltaTime();
			if(bird->state == FALLING)
			if(transform->position.y <= 0) {
				transform->position.y = 0;
				bird->velocity.y = 0;
				bird->state = IDLE;
			}
		break;
	}
}

void system_bird_renderer(hge_entity* entity, hge_transform* transform, character_bird* bird) {

	float t_offset = 10;
	float speed = 20.0f;
	switch(bird->state) {
		case IDLE:
		case FALLING:
		case FLYING:
		case RACE_COUNTDOWN:
			bird->rendered_transform.position.x = transform->position.x;
			bird->rendered_transform.position.y += (transform->position.y - bird->rendered_transform.position.y) * speed * hgeDeltaTime();
			bird->rendered_transform.position.z = transform->position.z;

			bird->rendered_transform.rotation.z += (transform->rotation.z - bird->rendered_transform.rotation.z) * speed * hgeDeltaTime();
			//printf("rotate: %f\n", bird->angle_overflow);
			if(bird->state == FLYING)
				bird->flip_h = (bird->angle - bird->angle_offset + 90 < 90 || bird->angle - bird->angle_offset + 90 > 270);
		break;
		case WALKING:
			bird->flip_h = (bird->rendered_transform.position.x > transform->position.x);

			bird->rendered_transform.position.x = transform->position.x;
			bird->rendered_transform.position.y += ((transform->position.y + fabs(sin(hgeRuntime()*t_offset)*5)) - bird->rendered_transform.position.y) * speed * hgeDeltaTime();
			bird->rendered_transform.position.z = transform->position.z;

			bird->rendered_transform.rotation.z += ((cos(hgeRuntime()*t_offset)*12) - bird->rendered_transform.rotation.z) * speed * hgeDeltaTime();
		break;
	}

	if(bird->state != SELECTING_LEVEL) {
		jamTextRenderSimple(
			getPlayerName(), true,
			hgeVec3(bird->rendered_transform.position.x, 15 + bird->rendered_transform.position.y-0.5f, 0.9f),
			0.1f,
			hgeVec4(0, 0, 0, 1)
		);
		// Text
		jamTextRenderSimple(
			getPlayerName(), true,
			hgeVec3(bird->rendered_transform.position.x, 15 + bird->rendered_transform.position.y, 2),
			0.1f,
			hgeVec4(1, 1, 1, 1)
		);
	}

  char hat_texture_name[255];
  char head_texture_name[255];

	hge_texture bird_body_texture = hgeResourcesQueryTexture("BIRD BODY");

  sprintf(&head_texture_name, "BIRD HEAD %d", getPlayerHead());
  hge_texture bird_head_texture = hgeResourcesQueryTexture(head_texture_name);

  sprintf(&hat_texture_name, "BIRD HAT %d", getPlayerHat());
  hge_texture bird_hat_texture = hgeResourcesQueryTexture(hat_texture_name);

	if(bird->state == FLYING || bird->state == RACE_COUNTDOWN || bird->state == RACE_END) {
		bird_body_texture = hgeResourcesQueryTexture("BIRD BODY WINGS OPENED");

    sprintf(&head_texture_name, "BIRD HEAD %d WINGS", getPlayerHead());
    bird_head_texture = hgeResourcesQueryTexture(head_texture_name);

    sprintf(&hat_texture_name, "BIRD HAT %d FLIGHT", getPlayerHat());
    bird_hat_texture = hgeResourcesQueryTexture(hat_texture_name);

		if(hgeInputGetMouse(HGE_MOUSE_LEFT))
			bird_body_texture = hgeResourcesQueryTexture("BIRD BODY WINGS CLOSED");
	}

	hge_vec3 rendered_scale = transform->scale;
	if(bird->flip_h) rendered_scale.x = -rendered_scale.x;

  hgeUseShader(hgeResourcesQueryShader("bird"));
  hgeShaderSetVec3(hgeResourcesQueryShader("bird"), "body_color", bird->body_color);
  if(bird->highlight_color.x == 0 && bird->highlight_color.y == 0 && bird->highlight_color.z == 0)
    hgeShaderSetVec3(hgeResourcesQueryShader("bird"), "highlight_color", bird->body_color);
  else
    hgeShaderSetVec3(hgeResourcesQueryShader("bird"), "highlight_color", bird->highlight_color);

  hge_material material = { bird_head_texture, hgeResourcesQueryTexture("HGE DEFAULT NORMAL") };

  // HAT
  material.diffuse = bird_hat_texture;
  if(getPlayerHat())
    hgeRenderSprite(hgeResourcesQueryShader("sprite_shader"), material, bird->rendered_transform.position, rendered_scale, bird->rendered_transform.rotation.z);

  // HEAD
  material.diffuse = bird_head_texture;
  hgeRenderSprite(hgeResourcesQueryShader("bird"), material, bird->rendered_transform.position, rendered_scale, bird->rendered_transform.rotation.z);
  // BODY
	material.diffuse = bird_body_texture;
	hgeRenderSprite(hgeResourcesQueryShader("bird"), material, bird->rendered_transform.position, rendered_scale, bird->rendered_transform.rotation.z);
}

int selected_option = 0;
int selected_level = 0;
void system_bird_controller(hge_entity* entity, hge_transform* transform, character_bird* bird) {

	if(hgeInputGetKeyDown(HGE_KEY_R)) {
		level_reload();
		return;
	}
  float camera_x_offset_for_menues = 0.0f;
	float camera_y_offset_for_menues = 0.0f;

	float move_speed = 50;
	switch(bird->state) {
		case IDLE:
			bird->velocity = hgeVec3(0, 0, 0);
		case WALKING:
			if(hgeInputGetKey(HGE_KEY_A) || hgeInputGetKey(HGE_KEY_D)) {
				bird->state = WALKING;
			} else {
				bird->state = IDLE;
				transform->rotation.z = 0;
			}

			if(hgeInputGetKey(HGE_KEY_A)) {
				bird->velocity.x = -move_speed;
				transform->position.x -= move_speed*hgeDeltaTime();
			}
			if(hgeInputGetKey(HGE_KEY_D)) {
				bird->velocity.x = move_speed;
				transform->position.x += move_speed*hgeDeltaTime();
			}

			/*if(hgeInputGetKeyDown(HGE_KEY_SPACE)) {
				bird->velocity.y = 250.f;
				bird->state = FALLING;
			}*/
			//printf("x: %f\n", transform->position.x);
			// WALLS
			if(transform->position.x < 159) transform->position.x = 159;
			if(transform->position.x > 408) transform->position.x = 408;

			// TAKE OFF
			if(transform->position.x > 251 && transform->position.x < 338) {
				bird->desired_fov = (1.f/6.f);
				//printf("could take off...\n");
				if(hgeInputGetKeyDown(HGE_KEY_SPACE)) {
					bird->state = SELECTING_LEVEL;
					//level_load("res/levels/bird_level_1.tmx");
				}
			} else {
				bird->desired_fov = (1.f/10.f);

        if(transform->position.x < 225 && hgeInputGetKeyDown(HGE_KEY_SPACE)) {
          bird->state = MENU_HATS;
        }
        if(transform->position.x > 350 && hgeInputGetKeyDown(HGE_KEY_SPACE)) {
          bird->state = MENU_BARBER;
        }
      }

		break;
		case FALLING:
			if(hgeInputGetMouseDown(HGE_MOUSE_LEFT)) {
				bird->state = FLYING;
			}
		break;
		case FLYING:
		{
			// Mouse Controls
			hge_vec3 mouse_pos = hgeInputGetMousePosition();
			mouse_pos.x =  mouse_pos.x - hgeWindowWidth()/2;
			mouse_pos.y = (mouse_pos.y - hgeWindowHeight()/2);
			float angle = atan2(-mouse_pos.y, mouse_pos.x);
			angle = angle * (180/M_PI);
			angle = -angle;
			angle += 90;
			if(!hgeInputGetMouse(HGE_MOUSE_LEFT))
			bird->desired_angle = angle;

			// Keyboard Controls
			/*if(hgeInputGetKey(HGE_KEY_W)) {
				bird->angle += 360*hgeDeltaTime();
			} if(hgeInputGetKey(HGE_KEY_S)) {
				bird->angle -= 360*hgeDeltaTime();
			}*/
		}
		break;
		case SELECTING_LEVEL:
		{
			camera_y_offset_for_menues = 20;

			int num_levels = num_level_files();

			bird->desired_fov = (1.f/12.f);
			if(hgeInputGetKeyDown(HGE_KEY_ESCAPE)) {
				bird->state = IDLE;
			} else if(hgeInputGetKeyDown(HGE_KEY_SPACE)) {
				char level_to_load[255];
				sprintf(level_to_load, "%s%s%s", "res/levels/", get_level_title(selected_level), ".tmx");
				level_load(level_to_load);
        bird->state = RACE_COUNTDOWN;
			}
			if(hgeInputGetKeyDown(HGE_KEY_W)) {
				selected_level++;
			} else if(hgeInputGetKeyDown(HGE_KEY_S)) {
				selected_level--;
			}
			selected_level = fmin(fmax(selected_level, 0), num_levels-1);

			for(int i = 0; i < num_levels; i++) {
				char level_name[255];
				jam_time jt = float_to_int_time(get_level_data(i).time);
				sprintf(level_name, "%s - %02d:%02d.%03d", get_level_title(i), jt.minutes, jt.seconds, jt.miliseconds);
				float text_y = 25 + transform->position.y + 5*i - selected_level*5;
				float distance_from_bird = 25+transform->position.y - text_y;

				// Shadow
				jamTextRenderSimple(
					level_name, true, hgeVec3(transform->position.x, text_y-0.5f, 0.9f),
					0.1,
					hgeVec4(0, 0, 0, 1 - fabs((distance_from_bird/15.f)))
				);
				// Text
				jamTextRenderSimple(
					level_name, true, hgeVec3(transform->position.x, text_y, 1),
					0.1,
					hgeVec4(1, 1, 1, 1 - fabs((distance_from_bird/15.f)))
				);
			}
		}
		break;
    case MENU_BARBER:
    {
      camera_x_offset_for_menues = 40;
      if(hgeInputGetKeyDown(HGE_KEY_ESCAPE)) {
				bird->state = IDLE;
      }

      hge_vec3 feather_color = getPlayerBodyColor();
      hge_vec3 highlight_color = getPlayerHighlightColor();

      bird->body_color = feather_color;
      bird->highlight_color = highlight_color;

      int num_options = 7;
      char options[num_options][20];
      sprintf(options[0], "Feathers (r) %d", (int)(feather_color.x*255.f));
      sprintf(options[1], "Feathers (g) %d", (int)(feather_color.y*255.f));
      sprintf(options[2], "Feathers (b) %d", (int)(feather_color.z*255.f));

      sprintf(options[3], "Highlights (r) %d", (int)(highlight_color.x*255.f));
      sprintf(options[4], "Highlights (g) %d", (int)(highlight_color.y*255.f));
      sprintf(options[5], "Highlights (b) %d", (int)(highlight_color.z*255.f));

      sprintf(options[6], "Style %d", getPlayerHead());

      if(hgeInputGetKeyDown(HGE_KEY_W)) {
				selected_option++;
			} else if(hgeInputGetKeyDown(HGE_KEY_S)) {
				selected_option--;
			}

      if(hgeInputGetKeyDown(HGE_KEY_A)) {
        switch(selected_option) {
          case 0:
            feather_color.x-=15.f/255.f;
            setPlayerBodyColor(feather_color);
          break;
          case 1:
            feather_color.y-=15.f/255.f;
            setPlayerBodyColor(feather_color);
          break;
          case 2:
            feather_color.z-=15.f/255.f;
            setPlayerBodyColor(feather_color);
          break;
          case 3:
            highlight_color.x-=15.f/255.f;
            setPlayerHighlightColor(highlight_color);
          break;
          case 4:
            highlight_color.y-=15.f/255.f;
            setPlayerHighlightColor(highlight_color);
          break;
          case 5:
            highlight_color.z-=15.f/255.f;
            setPlayerHighlightColor(highlight_color);
          break;
          case 6: // style
            setPlayerHead(getPlayerHead()-1);
          break;
        }
      } else if(hgeInputGetKeyDown(HGE_KEY_D)) {
        switch(selected_option) {
          case 0:
            feather_color.x+=15.f/255.f;
            setPlayerBodyColor(feather_color);
          break;
          case 1:
            feather_color.y+=15.f/255.f;
            setPlayerBodyColor(feather_color);
          break;
          case 2:
            feather_color.z+=15.f/255.f;
            setPlayerBodyColor(feather_color);
          break;
          case 3:
            highlight_color.x+=15.f/255.f;
            setPlayerHighlightColor(highlight_color);
          break;
          case 4:
            highlight_color.y+=15.f/255.f;
            setPlayerHighlightColor(highlight_color);
          break;
          case 5:
            highlight_color.z+=15.f/255.f;
            setPlayerHighlightColor(highlight_color);
          break;
          case 6: // style
            setPlayerHead(getPlayerHead()+1);
          break;
        }
      }

			selected_option = fmin(fmax(selected_option, 0), num_options-1);

      for(int i = 0; i < num_options; i++) {
				float text_y = transform->position.y + 5*i - selected_option*5;
				float distance_from_bird = transform->position.y - text_y;

				// Shadow
				jamTextRenderSimple(
					options[i], false, hgeVec3(415, text_y-0.5f, 0.9f),
					0.1,
					hgeVec4(0, 0, 0, 1 - fabs((distance_from_bird/15.f)))
				);
				// Text
				jamTextRenderSimple(
					options[i], false, hgeVec3(415, text_y, 1),
					0.1,
					hgeVec4(1, 1, 1, 1 - fabs((distance_from_bird/15.f)))
				);
			}
    }
    break;
    case MENU_HATS:
    {
      camera_x_offset_for_menues = -40;
      if(hgeInputGetKeyDown(HGE_KEY_ESCAPE)) {
				bird->state = IDLE;
      }

      if(hgeInputGetKeyDown(HGE_KEY_A)) {
        setPlayerHat(getPlayerHat()-1);
      }
      if(hgeInputGetKeyDown(HGE_KEY_D)) {
        setPlayerHat(getPlayerHat()+1);
      }

      float text_y = transform->position.y;

      char hat_str[10];
      if(getPlayerHat())
        sprintf(&hat_str, "hat %d", getPlayerHat());
      else
        sprintf(&hat_str, "no hat");

      // Shadow
      jamTextRenderSimple(
        hat_str, false, hgeVec3(135, text_y-0.5f, 0.9f),
        0.1,
        hgeVec4(0, 0, 0, 1)
      );
      // Text
      jamTextRenderSimple(
        hat_str, false, hgeVec3(135, text_y, 1),
        0.1,
        hgeVec4(1, 1, 1, 1)
      );
    }
    break;
    default:
    break;
	}
	transform->rotation.z = bird->angle;

	hge_camera* camera = NULL;
	hge_vec3* camera_position = NULL;
	hge_ecs_request active_camera_request = hgeECSRequest(1, "ActiveCamera");
	for(int i = 0; i < active_camera_request.NUM_ENTITIES; i++) {
		hge_entity* cam_entity = active_camera_request.entities[i];
		camera = cam_entity->components[hgeQuery(cam_entity, "Camera")].data;
		camera_position = cam_entity->components[hgeQuery(cam_entity, "Position")].data;
	}

	float total_speed = fabs(bird->velocity.x) + fabs(bird->velocity.y);

	float distance = 100.0f;
	if(bird->state != FLYING) distance = 0.0f;

	float desired_cam_x = transform->position.x+camera_x_offset_for_menues + sinf(hgeMathRadians(bird->angle))*distance;
	float desired_cam_y = transform->position.y+camera_y_offset_for_menues + cosf(hgeMathRadians(bird->angle))*distance;
	float cam_speed = 10;
  if(bird->state == FLYING) {
    cam_speed = 25;
  }
	camera_position->x += (desired_cam_x - camera_position->x) * cam_speed * hgeDeltaTime();
	camera_position->y += (desired_cam_y - camera_position->y) * cam_speed * hgeDeltaTime();
	//camera_position->x = desired_cam_x;
	//camera_position->y = desired_cam_y;

	if(bird->state == FLYING || bird->state == RACE_COUNTDOWN) {
		//camera->fov = (1.f/3.f);
		//printf("speed: %f\n", total_speed);
		//camera->fov = (1.f/6) * fmax(2.0f, 1.75f + 0.5f*fabs(total_speed/500.f));
		bird->desired_fov = (1.f/6) * fmax(2.0f, 1.75f + 0.5f*fabs(total_speed/500.f));
	}
	float fov_speed = 3;
	camera->fov += (bird->desired_fov - camera->fov) * fov_speed * hgeDeltaTime();

}
