#ifndef BACKGROUND_H
#define BACKGROUND_H
#include <HGE/HGE_Core.h>
#include <stdio.h>
#include <math.h>
#include "jamutils.h"

typedef struct {
	float parallax_amnt;
} parallax_component;

void system_parallax(hge_entity* entity, hge_transform* transform, hge_texture* sprite, parallax_component* parallax);

#endif
