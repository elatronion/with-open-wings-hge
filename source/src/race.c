#include "race.h"
#include "bird.h"
#include <HGE/HGE_GameMath.h>
#include "text.h"

void push_checkpoint(checkpoint* root_checkpoint, hge_vec3 location) {
	checkpoint* cur = root_checkpoint->next;
	if(!cur) {
		root_checkpoint->next = malloc(sizeof(checkpoint));
		root_checkpoint->next->location = location;
		root_checkpoint->next->next = NULL;
		return;
	}
	while(cur->next) cur = cur->next;
	cur->next = malloc(sizeof(checkpoint));
	cur->next->location = location;
	cur->next->next = NULL;
}

void clean_checkpoints(checkpoint* root_checkpoint) {
	checkpoint* cur = root_checkpoint->next;
	while(cur) {
		checkpoint* next = cur->next;
		free(cur);
		cur = next;
	}
}

void race_system(hge_entity* entity, race_component* race) {
	// Rendering
	float distance = 64;
	for(int i = 0; i < 16; i++) {
		float angle = i * 22.5f + hgeRuntime()*75;
		hge_vec3 feather_position =
		hgeVec3(
			race->cur_checkpoint->location.x + cos(hgeMathRadians(angle))*distance,
			race->cur_checkpoint->location.y + sin(hgeMathRadians(angle))*distance,
			-1
		);
		hge_material material = { hgeResourcesQueryTexture("FEATHER"), hgeResourcesQueryTexture("HGE DEFAULT NORMAL") };
		hgeRenderSprite(hgeResourcesQueryShader("sprite_shader"), material, feather_position, hgeVec3(16, 16, 0), 0.0f);
	}


	hge_transform* player_transform = NULL;
	character_bird* player_bird = NULL;
	hge_ecs_request player_request = hgeECSRequest(1, "player");
	if(player_request.NUM_ENTITIES < 1) return;
	for(int i = 0; i < player_request.NUM_ENTITIES; i++) {
		hge_entity* player_entity = player_request.entities[i];
		player_transform = player_entity->components[hgeQuery(player_entity, "transform")].data;
		player_bird = player_entity->components[hgeQuery(player_entity, "bird")].data;
	}

	if(player_bird->state == RACE_END) {

		jam_time jt = float_to_int_time(race->timer);
		char timer_str[255];
		sprintf(timer_str, "%02d:%02d.%03d", jt.minutes, jt.seconds, jt.miliseconds);
		// Shadow
		jamTextRenderSimple(
			timer_str, true,
			hgeVec3(player_transform->position.x, 25 + player_transform->position.y-0.5f, 0.9f),
			0.25f,
			hgeVec4(0, 0, 0, 1)
		);
		// Text
		jamTextRenderSimple(
			timer_str, true,
			hgeVec3(player_transform->position.x, 25 + player_transform->position.y, 2),
			0.25f,
			hgeVec4(1, 1, 1, 1)
		);

		// Shadow
		jamTextRenderSimple(
			"r - restart", true,
			hgeVec3(player_transform->position.x, -25 + player_transform->position.y-0.5f, 0.9f),
			0.25f,
			hgeVec4(0, 0, 0, 1)
		);
		// Text
		jamTextRenderSimple(
			"r - restart", true,
			hgeVec3(player_transform->position.x, -25 + player_transform->position.y, 2),
			0.25f,
			hgeVec4(1, 1, 1, 1)
		);

		// Shadow
		jamTextRenderSimple(
			"space - continue", true,
			hgeVec3(player_transform->position.x, -35 + player_transform->position.y-0.5f, 0.9f),
			0.25f,
			hgeVec4(0, 0, 0, 1)
		);
		// Text
		jamTextRenderSimple(
			"space - continue", true,
			hgeVec3(player_transform->position.x, -35 + player_transform->position.y, 2),
			0.25f,
			hgeVec4(1, 1, 1, 1)
		);

		if(hgeInputGetKeyDown(HGE_KEY_SPACE)) {
			load_bird_place();
		}

		return;
	}

	// COUNTDOWN
	if(race->countdown > 0) {
		char count_down_str[255];
		sprintf(&count_down_str, "%d", (int)race->countdown+1);
		// Shadow
		jamTextRenderSimple(
			count_down_str, true,
			hgeVec3(player_transform->position.x, 25 + player_transform->position.y-0.5f, 0.9f),
			0.25f,
			hgeVec4(0, 0, 0, 1)
		);
		// Text
		jamTextRenderSimple(
			count_down_str, true,
			hgeVec3(player_transform->position.x, 25 + player_transform->position.y, 2),
			0.25f,
			hgeVec4(1, 1, 1, 1)
		);
	}

	float checkpoint_direction = (atan2(player_transform->position.y - race->cur_checkpoint->location.y, player_transform->position.x - race->cur_checkpoint->location.x));
	checkpoint_direction = checkpoint_direction * (180/M_PI);
	checkpoint_direction = -checkpoint_direction;
	checkpoint_direction -= 90;
	hge_material material = { hgeResourcesQueryTexture("FEATHER"), hgeResourcesQueryTexture("HGE DEFAULT NORMAL") };
	hgeRenderSprite(hgeResourcesQueryShader("gui"), material, hgeVec3(0, 0, 0), hgeVec3(16, 16, 0), checkpoint_direction);

	// Race System

	hge_transform checkpoint_transform;
	checkpoint_transform.position = race->cur_checkpoint->location;
	checkpoint_transform.scale = hgeVec3(distance*2, distance*2, 0);
	if(AABB(*player_transform, checkpoint_transform)) {
		// Intersection
		printf("Intersection\n");
		race->cur_checkpoint = race->cur_checkpoint->next;
		if(!race->cur_checkpoint) {
			race->cur_checkpoint = race->root_checkpoint->next;
			player_bird->state = RACE_END;

			// Check Record
			char title[50];
			sscanf(cur_level_str(), "res/levels/%[^\n].tmx", &title);
			title[strlen(title)-4] = '\0';
			char path[255];
			sprintf(path, "%s%s%s", "res/levels/scores/", title, ".bin");
			level_data lvl_data = { 0.0f };
			binread(path, &lvl_data, sizeof(lvl_data));

			if(race->timer < lvl_data.time || lvl_data.time == 0) {
				// new record
				lvl_data.time = race->timer;
				binwrite(path, &lvl_data, sizeof(lvl_data));
				clean_loaded_level_files();
				load_level_folder();
			}

		}
	}

	if(race->countdown <= 0) {
		race->timer += hgeDeltaTime();
		if(player_bird->state != RACE_END)
			player_bird->state = FLYING;
	} else race->countdown -= hgeDeltaTime();
}
