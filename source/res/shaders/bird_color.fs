#version 330 core
out vec4 FragColor;

in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoord;

struct Material {
  sampler2D diffuse;
  sampler2D normal;
};

uniform Material material;

uniform vec3 body_color;
uniform vec3 highlight_color;

void main() {
  FragColor = texture(material.diffuse, TexCoord);
  /*if(FragColor.r > 0 && FragColor.g == 0 && FragColor.b == 0) {
    FragColor = vec4(0, 0, 1, 1);
  }*/

  //vec3 color_r = vec3(50.f/255.f, 132/255.f, 212.f/255.f);
  //vec3 color_g = vec3(176.f/255.f, 70.f/255.f, 199.f/255.f);
  vec3 color_r = body_color;
  vec3 color_g = highlight_color;

  FragColor.r =
  texture(material.diffuse, TexCoord).r * color_r.r +
  texture(material.diffuse, TexCoord).g * color_g.r
  ;

  FragColor.g =
  texture(material.diffuse, TexCoord).r * color_r.g +
  texture(material.diffuse, TexCoord).g * color_g.g
  ;

  FragColor.b =
  texture(material.diffuse, TexCoord).r * color_r.b +
  texture(material.diffuse, TexCoord).g * color_g.b
  ;

  if(
    texture(material.diffuse, TexCoord).r > 0 &&
    texture(material.diffuse, TexCoord).g > 0 &&
    texture(material.diffuse, TexCoord).b > 0
    ) {
    FragColor = texture(material.diffuse, TexCoord);
  }

  if(FragColor.a == 0) discard;
}
