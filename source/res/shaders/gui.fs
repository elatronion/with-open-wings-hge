#version 330 core
out vec4 FragColor;

in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoord;

struct Material {
  sampler2D diffuse;
  sampler2D normal;
};

uniform Material material;

void main() {
  FragColor = texture(material.diffuse, TexCoord);
  if(FragColor.a == 0) discard;
}
