#version 330 core
layout (location = 0) in vec3 aPos;   // the position variable has attribute position 0
layout (location = 1) in vec2 aTexCoord;
layout (location = 2) in vec3 aNormal;

out vec3 ourColor; // output a color to the fragment shader
out vec2 TexCoord;

out vec3 FragPos;
out vec3 Normal;

uniform mat4 model;
uniform mat4 projection;

void main()
{

    gl_Position = projection * model * vec4(aPos, 1.0);
    TexCoord = vec2(aTexCoord.x, 1.0-aTexCoord.y);

    FragPos = vec3(model * vec4(aPos, 1.0));
    Normal = aNormal;
}
